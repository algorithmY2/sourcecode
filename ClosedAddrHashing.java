import java.util.Scanner;

public class ClosedAddrHashing {
	public static void main(String[] args) {

		int j, key = 0, data = 0, userKey = 0, userData = 0;

		HashMap hmap = new HashMap();

		System.out.println("Big data generating...please wait 1 mins.");

		for (j = 0; j <= 250000; j++) {

			// Create a NRIC(key) between 0 ~ 9999999
			// Car plate No.(data) between 0 ~ 99999999
			// To find the different between prime and non-prime No.
			// Use special case: numbers with multiple of 4.

			key = 4 * (int) Math.floor(Math.random() * 2499999 + 1);

			data = (int) Math.floor(Math.random() * 99999999 + 1);

			hmap.put(key, data);
		}

		System.out.println("Data adding finished.");
		System.out.println("Lastest input is [key:" + key + ", Data:" + data + "]");
		System.out.println("=================================");
		while (true) {
			System.out.println("Please key in a NRIC you want to looking for :");
			Scanner sc = new Scanner(System.in);
			userKey = sc.nextInt();
			if (userKey == -1)//input -1 to terminate the program
				break;

			// calculate CPU time
			long start = 0;

			int runs = 10000; // enough to run for 2-10 seconds. Avoid warm up time.

			for (int i = -10000; i < runs; i++) {

				if (i == 0)
					start = System.nanoTime();

				// do test
				userData = hmap.get(userKey);

			}

			long time = System.nanoTime() - start;
			
			//get number of comparisions
			userData = hmap.getNCount(userKey);

			if (userData != -1)
				System.out.println("The car plate No. founded, it is " + userData);

			else
				System.out.println("No record of this NRIC No.");

			System.out.printf("The searching took an average of %,d ns%n", time / runs);
			System.out.printf("The searching took an average of %,d comparisions%n%n", hmap.getNoOfComp());
			
			hmap.setNoOfComp();//reset the count of comparisons
		}
	}
}
